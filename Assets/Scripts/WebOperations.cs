﻿using System.IO;
using System.Net;
using Newtonsoft.Json;

public class WebOperations
{
    public const string SpriteRefences = "https://drive.google.com/uc?export=download&id=1Dzf0ItlZ8HqO6lSmUyC-EGst24njXASO";
    
    public Reference GetReferences()
    {
        var request = WebRequest.Create(SpriteRefences);
        var response = request.GetResponse();
        using (var stream = response.GetResponseStream())
        {
            using (var file = new StreamReader(stream))
                return (Reference)JsonConvert.DeserializeObject(file.ReadToEnd(), typeof(Reference));
        }
    }
}

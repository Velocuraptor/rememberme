﻿using UnityEngine;
using UnityEngine.UI;

public class Counter : MonoBehaviour
{
    [SerializeField] private Text _score;

    private int _points;

    public void AddPoint()
    {
        _points += 1;
        UpdateUI();
    }

    public void ResetScore()
    {
        _points = 0;
        UpdateUI();
    }

    private void UpdateUI()
    {
        _score.text = _points.ToString();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardManager : MonoBehaviour
{
    [SerializeField] private float _timeRemember = 5.0f;
    [SerializeField] private float _timeShowMistake = 1.0f;
    [SerializeField] private float _timeShowCorrectly = 1.0f;
    [Space]
    [SerializeField] private List<Card> _cards;
    [SerializeField] private SpriteDownloader _spriteDownloader;
    [SerializeField] private Counter _counter;

    private List<Card> _cardsInGame;

    public void Start()
    {
        GetSprites();
    }

    private void GetSprites()
    {
        _spriteDownloader.DownloadSprites(_cards.Count / 2);
    }

    public void StartGame(Sprite[] spritesFace, Sprite spriteBack)
    {
        _cardsInGame = new List<Card>(_cards);
        InitCards(spritesFace, spriteBack);
        StartCoroutine(ShowCards());
    }

    private void InitCards(Sprite[] spritesFace, Sprite spriteBack)
    {
        var cards = new List<Card>(_cards);
        var countCardPairs = cards.Count / 2;
        for (var i = 0; i < countCardPairs; i++)
        {
            for (var j = 0; j < 2; j++)
            {
                var cardId = Random.Range(0, cards.Count);
                cards[cardId].Init(i, spritesFace[i], spriteBack);
                cards.RemoveAt(cardId);
            }
        }
    }

    private IEnumerator ShowCards()
    {
        foreach (var card in _cards)
            card.RotateCard();

        yield return new WaitForSeconds(_timeRemember);

        foreach (var card in _cards)
            card.RotateCard();
    }

    public IEnumerator Сorrectly(Card[] cards)
    {
        _counter.AddPoint();

        foreach (var card in cards)
            _cardsInGame.Remove(card);

        foreach (var card in _cardsInGame)
            card.Interactable(false);

        yield return new WaitForSeconds(_timeShowCorrectly);

        foreach (var card in cards)
            card.gameObject.SetActive(false);

        foreach (var card in _cardsInGame)
            card.Interactable(true);

        if (_cardsInGame.Count <= 0)
            GetSprites();
    }


    public IEnumerator Mistake(Card[] cards)
    {
        foreach (var card in _cardsInGame)
            card.Interactable(false);

        yield return new WaitForSeconds(_timeShowMistake);

        foreach (var card in cards)
            card.RotateCard();

        foreach (var card in _cardsInGame)
            card.Interactable(true);
    }
}



﻿using System.Collections.Generic;
using System;

[Serializable]
public class Reference
{
    public List<string> FaceCardReferences;
    public List<string> BackCardReferences;

    public int FacesCount => FaceCardReferences.Count;
    public int BacksCount => BackCardReferences.Count;

    public Reference()
    {

    }
}

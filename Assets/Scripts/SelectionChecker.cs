﻿using UnityEngine;

public class SelectionChecker : MonoBehaviour
{
    [SerializeField] private CardManager _cardManager;
    private Card _activeCard;

    public void CardSelected(Card card)
    {

        if (_activeCard == null)
        {
            _activeCard = card;
            return;
        }

        if (card.Value != _activeCard.Value)
        {
            StartCoroutine(_cardManager.Mistake(new[] { _activeCard, card }));       
            _activeCard = null;
            return;
        }

        StartCoroutine(_cardManager.Сorrectly(new[] { _activeCard, card }));
        _activeCard = null;
    }

}

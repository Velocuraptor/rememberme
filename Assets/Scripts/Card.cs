﻿using UnityEngine;
using UnityEngine.Events;

public class Card : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private BoxCollider2D _collider;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private UnityEvent _selectCard;

    private Sprite _spriteFace;
    private Sprite _spriteBack;
    private int _valueCard;
    private bool _isActive;

    public int Value => _valueCard;

    public void RotateCard() => _animator.SetTrigger("Rotate");

    public void SwitchSprite()
    {
        if (_isActive)
            CloseCard();
        else
            ShowCard();        
    }

    public void Init(int valueCard, Sprite spriteFace, Sprite spriteBack)
    {
        _spriteFace = spriteFace;
        _spriteBack = spriteBack;
        _valueCard = valueCard;
        CloseCard();
        gameObject.SetActive(true);
    }

    private void ShowCard()
    {
        Interactable(false);
        _isActive = true;
        _spriteRenderer.sprite = _spriteFace;
    }

    private void CloseCard()
    {
        Interactable(true);
        _isActive = false;
        _spriteRenderer.sprite = _spriteBack;
    }

    public void Interactable(bool value) => _collider.enabled = value;

    private void OnMouseDown()
    {
        RotateCard();
        _selectCard?.Invoke();
    }
}

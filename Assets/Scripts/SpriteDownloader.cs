﻿using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class SpriteDownloader : MonoBehaviour
{
    [SerializeField] private CardManager _cardManager;
    private WebOperations _webOperations;
    private Reference _reference;

    private Sprite[] _spritesFace;
    private Sprite _spriteBack;

    public void Start()
    {
        _webOperations = new WebOperations();
        _reference = _webOperations.GetReferences();
    }

    public void DownloadSprites(int count)
    {
        _spritesFace = new Sprite[count];
        _spriteBack = null;

        var backFace = _reference.BackCardReferences[Random.Range(0, _reference.BacksCount)];
        var urlFace = _reference.FaceCardReferences.OrderBy(c => Random.Range(0, 100)).Take(count).ToArray();

        StartCoroutine(GetSprite(backFace));
        for (var i = 0; i < count; i++)
            StartCoroutine(GetSprite(urlFace[i], i));
    }

    public IEnumerator GetSprite(string url)
    {
        var request = UnityWebRequestTexture.GetTexture(url);

        yield return request.SendWebRequest();

        if (!request.isDone)
        {
            Debug.Log(request.error);
        }
        var texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
        _spriteBack = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.one * 0.5f, texture.width);

        CheckEnd();
    }

    public IEnumerator GetSprite(string url, int i)
    {
        var request = UnityWebRequestTexture.GetTexture(url);

        yield return request.SendWebRequest();

        if (!request.isDone)
        {
            Debug.Log(request.error);
        }
        var texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
        _spritesFace[i] = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.one * 0.5f, texture.width);

        CheckEnd();
    }

    private void CheckEnd()
    {
        if (_spriteBack == null)
            return;

        foreach (var sprite in _spritesFace)
        {
            if (sprite == null)
                return;
        }

        _cardManager.StartGame(_spritesFace, _spriteBack);
    }
}